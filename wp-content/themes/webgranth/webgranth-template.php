<?php
/*
Template Name: webgranth
*/
get_header();
?>


<div class="banner" >
	<div class="wrapper">
		<div class="banner-mod"></div>
	</div>
</div>



<div class="work">
	<div class="wrapper">
		<div class="work-mod">

			<h2>We Take your work to a new level</h2>

			<ul class="work-ul">
	

				
				<?php $work = get_field('work');
					foreach ($work as $value) { ?>
						
				
				<li class="work-li">
					<div class="work-image">
						<img src="<?php echo $value['work_image']['url'];?>">
					</div>
					<div class="work-detail">
						<h3> <?php echo $value['work_title'];?> </h3>
						<p> <?php echo $value['work_desc'];?></p>
					</div>
				</li>
			<?php	}
				?>
				




			</ul>
		</div>
	</div>
</div>
<div class="service">
	<div class="wrapper">
		<div class="service-mod">
	
			<h2>We Provide best services to a company</h2>
			
			<ul class="sevice-ul">
	

				
				<?php $service = get_field('services');
					foreach ($service as $value) { ?>
						
				
				<li class="service-li">

					<div class="service-sub">
						<div class="service_icon">
							<img src="<?php echo $value['service_icon']['url'];?>">	
						</div>
						<div class="service_title">
							<h3><?php echo $value['service_title'];?></h3>
							<p><?php echo $value['service_sub'];?></p>
						</div>
					</div>
					<div class="service_desc">
						<p> <?php echo $value['service_desc'];?></p>
					</div>
				</li>
			<?php	}
				?>
				




			</ul>
		</div>
	</div>
</div>





<?php
if (have_posts()) :
	while (have_posts()) : the_post(); ?>

	
<?php	
	endwhile;
	else :
		echo "<p class='error_not_found'> Nothing is there to display</p>";

	endif;		


get_footer();

?>