


<div class="info">
	<div class="wrapper">	
		<div class="info-mod">
			<div class="testimonal">
				<h3 class="info-title"> Testimonal </h3>	
				<?php 
					$testimonal = get_field('testimonal');
					foreach ($testimonal as $data) {?>
							<p><q> <?php echo $data['quote']; ?>  </q><br/>
							<i> <?php echo "- ".$data['author']; ?> </i>
							</p>	

				<?php	}

				?>


			</div>
			<div class="blog">

				<h3 class="info-title"> From the blog </h3>	
				<ul class="blog-ul">
				<?php 
					$blog = get_field('blog');

					foreach ($blog as $data) {?>
						<li class="blog-li">
							<div class="blog-item">
								<div class="item-image">
									<img src="<?php echo $data['article_logo']['url'];?>">
								</div>
								<div class="item-data">
									<p class="item-title"> <?php echo $data['article_title'];?></p>
									<p class="item-date"> <?php echo $data['date'];?></p>
								</div>
							</div>
						</li>							

				<?php	}

				?>

			 </ul>	
			</div>
			<div class="about-company">
				<h3 class="info-title"> About Company </h3>	
				<p class=""><?php echo get_field('about_company');?> </p>
				<h3 class="info-title"> Stay in Touch </h3>	
				<div class="social">
				<?php 
					$social = get_field('social');
					foreach ($social as $data) {?>
						<a href="<?php echo $data['s-link'];?>"> <img class="social-image" src="<?php echo $data['social_image']['url'];?>"></a>
					<?php } ?>	
				</div>		

		
			</div>
		</div>
	</div>
</div>










		<footer class="site-footer">
			<nav class="site-nav">
						<?php 
							$args =  array(
								'theme_location' => 'footer'
			 					);	
						?>
						<?php wp_nav_menu( $args ); ?>
			</nav>
			<div class="copyright">
				<p>2012&copy;companyname. All Rights Reserved</p>
			</div>


		</footer>

	</div>   
	<?php wp_footer();?>
	</body>
</html>