<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php bloginfo('name');?></title>


 

<?php wp_head();?>
</head>
<body <?php body_class(); ?> style="font-family: '<?php echo get_option('font');?>'">
<div class="container"> 
<header class="site-header">
	<div class="title">

		<img src="<?php echo get_field('logo')['url'];  ?>" width="150" height="70"><a href="<?php echo home_url(); ?>"> </a></h1>
		
	</div>
	<nav class="site-nav">
		<?php 

			$args =  array(
					   'theme_location' => 'primary'
					  );
		?>
	    <?php wp_nav_menu( $args ); ?>
	</nav>
</header>

