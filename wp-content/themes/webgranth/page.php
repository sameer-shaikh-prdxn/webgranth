<?php

/* INCLUDING HEADER */
get_header();

echo "On page.php";
/*  MAIN CONTENT */
if (have_posts()) :
	while (have_posts()) : the_post(); ?>
		<article class="post page">
		<?php if(has_children() || $post->post_parent > 0) {?>
			<nav class="site-nav children-links clearfix">
			<span class="parent-link"><a href="<?php echo get_the_permalink(get_top_ancestor_id());?>">
				<?php echo get_the_title(get_top_ancestor_id());?></a></span>	
					<ul class="other_links">
					<?php
						$args = array(
								'child_of' => get_top_ancestor_id(),
								'title_li' => ''
						);
					?>
					<?php wp_list_pages($args); ?>
				</ul>
			</nav>
		<?php } ?>

			<h2> <?php the_title();?> </h2>
			
			<?php if(is_page('7')) {
					$im = get_field('image');
			 ?>
			 <h3>  Image </h3>
			<img src="<?php echo $im['url']; ?>">

			<h3>  Editor </h3>
			
			<p> <?php the_field('editor'); ?></p>	
			<h3>  Oembed </h3>
			<div class="oembed">
					<?php the_field('oembed');?>
			</div>	

			<h3>  Image Gallery </h3>
			<div class="image-gallery">
<?php 

$images = get_field('gallery');
$size = 'full'; // (thumbnail, medium, large, full or custom size)

if( $images ): ?>


    <ul>
        <?php foreach( $images as $image ): ?>

            <li>
            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

			 </div>
		<p> <?php
 		}elseif( get_the_ID() == '297'){
 				//$news_repeater = get_field('slide');
 				echo "<ul>";

 				// check if the repeater field has rows of data
if( have_rows('news') ):

 	// loop through the rows of data
    while ( have_rows('news') ) : the_row();

    	$link = get_sub_field('heading');
    	$head = get_sub_field('heading_1');
        // display a sub field value
        echo "<li><h3><a href=$link> $head</a><h3></li>" ;

    endwhile;

else :

    // no rows found

endif;




 		}
 		elseif(get_the_ID() == '289'){	?>
		<?php	
		$slider_repeater = get_field('slide');
		// for(  $i=0;$i< sizeof($slider_repeater) ; $i++){

//?//>
?>
<div class="slider">
			

			<?php for($i = 0 ; $i < 3 ; $i++){
					$r = $slider_repeater[$i]['image']['url'];
					echo "<div><img src='$r'></div>";


			}?>
		<!-- $r = $slider_repeater[0]['image']['url']; -->
<!-- 			<div><img src="<?php echo $slider_repeater['0']['image']['url'];?>" ></div>
			<div><img src="<?php echo $slider_repeater['1']['image']['url'];?>" ></div>
 -->		<!-- }	 -->

</div>





 			<?php }elseif( get_the_ID() == '259'){?>


 			<h3> Date Picker</h3>
 			<?php 

			// get raw date
			$date = get_field('date_picker', false, false);


			// make date object
			$date = new DateTime($date);

			?>
			<p>Event start date: <?php echo $date->format('j M Y'); ?></p>



				<h3> Date Time Picker</h3>

			<p>Event starts: <?php the_field('dtp'); ?></p>
















<?php 			}




 			elseif( get_the_ID() == '241'){
 				
 				?>
 				<h3> Link Example</h3>
 				<a href=  "<?php the_field('link'); ?>"> Search On google</a>
				<h3> Post Object </h3>




<?php

$post_object = get_field('post_object');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>
    <div>
    	<h3><a href="<?php the_permalink(); ?>"><?php the_field('title'); ?></a></h3>
    	<span>Phone Number from custom fieldd post object: <?php the_field('phone'); ?></span>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
				



	

		<h3> Page Link example</h3>
		<a href="<?php the_field('page_link');?>"> Page Link</a>



		<h3> Relationship example</h3>	
		
		<?php 

$posts = get_field('relationship');

if( $posts ): ?>
    <ul>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <span>Custom Relationship field from $post: <?php the_field('user'); ?></span>
        </li>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>


		
		<h3> User Example</h3>
		<?php  
		$users = get_field('user');
		foreach ($users as $user) {
				echo $user;
		}

		  ?>



		  <h3> Taxonomy Example</h3>
		 <?php 

$terms = get_field('taxonomy');

if( $terms ): ?>

	<ul>

	<?php foreach( $terms as $term ): ?>

		<h2><?php echo $term->name; ?></h2>
		<p><?php echo $term->description; ?></p>
		<a href="<?php echo get_term_link( $term ); ?>">View all '<?php echo $term->name; ?>' posts</a>

	<?php endforeach; ?>

	</ul>

<?php endif; ?>


			

 			
 		<?php	} 






		elseif( get_the_ID() == '270'){?>
		<h3> The Repeater Field</h3>

		<?php if(get_field('repeater')): ?>

	<ul>

	<?php while(has_sub_field('repeater')): ?>

		<li>sub_field_1 = <?php the_sub_field('text'); ?>, sub_field_2 = <?php the_sub_field('text1'); ?>, etc</li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>







<?php
 		}


		else{
			 the_content();
		 	 }?>
 				 	 	
	 	 </p>


		</article>
 
<?php	
	endwhile;
	else :
		echo "<p class='error_not_found'> Nothing is there to display</p>";
	endif;		

/* END OF CONTENT */


/* INCLUDING FOOTER */
get_footer();

?>